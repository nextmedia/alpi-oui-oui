<?php

namespace AppBundle\Services;

class AESEncryptDecrypt
{
    private $method;
    private $hash;
    private $iv;

    private $base64;

    public function __construct(String $hash = null, String $iv = null)
    {
        //Password de 32 caractères & IV de 16
        $this->hash = $hash;
        $this->iv = $iv;

        //Methode utilisée
        $this->method = "AES-256-CBC";
    }

    public function setHashAndIv($hash, $iv){
        $this->hash = $hash;
        $this->iv = $iv;
    }

    public function setBase64(){
        //Si on utilise base64
        $this->base64 = true;
    }

    /**
     * Crypt.
     */
    public function encrypt($text)
    {
        $encrypt = openssl_encrypt($text, $this->method, $this->hash, 0, $this->iv);
        
        if($this->base64)
            $encrypt = base64_encode( $encrypt );

        return $encrypt;
    }

    /**
     * Decrypt.
     */
    public function decrypt($encrypted)
    {
        if($this->base64)
            $encrypted = base64_decode( $encrypted );

        return openssl_decrypt($encrypted, $this->method, $this->hash, 0, $this->iv);
    }

    /**
     * Crypt for token.
     * Utilisé pour ne pas avoir le caractère '/' dans l'url
     * Pour les token, nous n'utilisons que la fonction encrypt pour comparaison donc ce n'est pas génant de retirer le caractère '/'
     */
    public function encryptToken($text)
    {
        $encrypt = openssl_encrypt($text, $this->method, $this->hash, 0, $this->iv);
        $encrypt = str_replace("/", "", $encrypt);

        return $encrypt;
    }
}