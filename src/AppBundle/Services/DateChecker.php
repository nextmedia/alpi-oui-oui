<?php

namespace AppBundle\Services;

use Symfony\Component\Config\Definition\Exception\Exception;

class DateChecker
{
	/**
	 * Verify le format de la date ISO8601
	 */
	private function isISO8601Correct(String $dateString){
		if (preg_match('/^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/', $dateString) > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * 
	 */
	public function checkValidDate(String $date){
		if($this->isISO8601Correct($date)){
			$minusToday = (new \DateTime())->modify('-1 day');
	        $maxToday = (new \DateTime())->modify('+1 day');
	        $date = new \DateTime($date);

	        if($minusToday <= $date && $date <= $maxToday){
	        	return true;
	        }else{
				throw new Exception('ERROR [426] : DateTime is not in an acceptable range.', 426);
	        }
		}else{
			throw new Exception('ERROR [422] : Invalid DateTime format.', 422);
		}
        throw new Exception('ERROR [500] : An error occured during datetime verification.', 500);
    }
}