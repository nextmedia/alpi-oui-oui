<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PikadeliController
 * @package AppBundle\Controller
 */
class PikadeliController extends Controller
{
    /**
     * @Route("/pikadeli/db", name="pikadeli_oiazj")
     * @return Response
     */
    public function dbAction()
    {
        return new Response("ERROR [503] : Service Unavailable.", 503);
    }

    /**
     * @Route("/pikadeli/del", name="pikadeli_del_req")
     */
    public function delAction()
    {
        return new Response("ERROR [503] : Service Unavailable.", 503);
    }
}