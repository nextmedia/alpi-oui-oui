<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RecordController
 * @package AppBundle\Controller
 * @Route("/record")
 */
class RecordController extends Controller
{
	/**
     * @Route("/user_pc", name="record_user_pc")
     */
	public function recordUserPcAction()
	{
        return new Response("ERROR [503] : Service Unavailable.", 503);
	}
}