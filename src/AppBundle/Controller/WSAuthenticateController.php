<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class WSAuthenticateController
 * @package AppBundle\Controller
 */
class WSAuthenticateController extends Controller
{
	/**
     * @Route("/authenticate", name="ws_authenticate")
     */
	public function authenticateAction()
	{
        return new Response("ERROR [503] : Service Unavailable.", 503);
	}
}