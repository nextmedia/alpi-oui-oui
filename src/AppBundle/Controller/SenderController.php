<?php

namespace AppBundle\Controller;

use AppBundle\Services\DateChecker;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SenderController
 * @package AppBundle\Controller
 */
class SenderController extends Controller
{
    const HASHKEY = 'aeb02ae89666a230cffa01d9d774cf01';
    const IV = 'b33d46f0cf33f347';

    /**
     * @Route("/req", name="req")
     * @param Request $request
     * @param DateChecker $dateChecker
     * @return Response
     */
    public function indexAction(Request $request, DateChecker $dateChecker)
    {
        $AESEncryptDecrypt = $this->get('AESEncryptDecrypt');
        $AESEncryptDecrypt->setHashAndIv(self::HASHKEY, self::IV);

        $xmlRequest = $this->extractFileToXML($request->files);

        $rqtInfo = [
            "datetime" => (string)$xmlRequest->action->datetime,
            "rqtType" => (string)$xmlRequest->action->type,
            "rqtVersion" => $xmlRequest->action->logiciel ? 'V1' : ($xmlRequest->action->produit ? 'V2' : -1),
            "emprunt" => (string)$xmlRequest->action->emprunt,
            "idpcToken" => (string)$xmlRequest->action->idpc,
            "idUserToken" => (string)$xmlRequest->action->idUser,
            "logicielToken" => (string)$xmlRequest->action->logiciel,
            "produitToken" => (string)$xmlRequest->action->produit,
            "varianteToken" => (string)$xmlRequest->action->variante,
            "moduleProduitToken" => (string)$xmlRequest->action->moduleProduit,
            "numeroToken" => (string)$xmlRequest->action->numero,
            "versionToken" => (string)$xmlRequest->action->version,
            "numCleToken" => (string)$xmlRequest->action->numCle
        ];

        if (strcasecmp($rqtInfo["rqtType"], "free") == 0 || !empty($rqtInfo["emprunt"])) {
            return new Response("ERROR [503] : Service Unavailable.", 503);
        }

        $xmlResponse = "<?xml version='1.0' encoding='UTF-8'?>";
        $xmlResponse .= "<licences idpc='" . $rqtInfo["idpcToken"] . "' iduser='" . $rqtInfo["idUserToken"] . "' mode='degrade'";
        if (!empty($rqtInfo["datetime"]))
            $xmlResponse .= " datetime='" . $rqtInfo["datetime"] . "' ";
        $xmlResponse .= ">";
        $xmlResponse .= "<licence>";
        $xmlResponse .= "<logiciel>";
        if ($rqtInfo["numCleToken"]) {
            $xmlResponse .= "<numCle>" . $rqtInfo["numCleToken"] . "</numCle>";
        }
        $xmlResponse .= "<produit>" . $rqtInfo["produitToken"] . "</produit>";
        if ($rqtInfo["moduleProduitToken"]) {
            $xmlResponse .= "<modules>";
            $xmlResponse .= "<module produit='" . $rqtInfo["moduleProduitToken"] . "'>" . $rqtInfo["moduleProduitToken"] . "</module>";
            $xmlResponse .= "</modules>";
        }
        $xmlResponse .= "</logiciel>";
        $xmlResponse .= "</licence>";

        $xmlResponse = $xmlResponse . "</licences>";

        return new Response($AESEncryptDecrypt->encrypt($xmlResponse), 200);
    }

    /**
     * @Route("/ping", name="ping")
     * @param Request $request
     * @return Response
     */
    public function pingAction(Request $request)
    {
        $AESEncryptDecrypt = $this->get('AESEncryptDecrypt');

        $files = $request->files;
        if (!sizeof($files)) {
            return new Response('ERROR [406] : Request file not found.', 406);
        }

        $file = array_values($files->all())[0];
        $extension = $file->getClientOriginalExtension();
        if ($extension == "xml" || $extension == "rqt") {
            $filePath = $file->getPathname();
            if ($extension == "xml") {
                $xmlRequest = simplexml_load_file($filePath);
            } else {
                $AESEncryptDecrypt->setHashAndIv(self::HASHKEY, self::IV);
                $xmlRequest = $this->extractFileToXML($files);
            }

            if ($xmlRequest->action->type == 'PING') {
                /**********************************
                 ** Construction du résultat XML **
                 **********************************/
                $xmlResponse = $this->pingToXml();
                if ($xmlRequest->action->encrypt == "true") {
                    $AESEncryptDecrypt->setHashAndIv(self::HASHKEY, self::IV);
                    $xmlResponse = $AESEncryptDecrypt->encrypt($xmlResponse);
                }

                return new Response($xmlResponse);
            }
            return new Response('ERROR [422] : This is not a PING request.', 422);
        }
        return new Response('ERROR [409] : .xml or .rqt format file expected, .' . $file->getClientOriginalExtension() . ' file received.', 409);
    }

    /**
     * @param $file
     * @return \SimpleXMLElement
     */
    private function extractFileToXML($file)
    {
        $AESEncryptDecrypt = $this->get('AESEncryptDecrypt');

        $filePath = array_values($file->all())[0]->getPathname();
        $fileContent = file_get_contents($filePath);
        $fileContent = $AESEncryptDecrypt->decrypt($fileContent);

        return simplexml_load_string($fileContent);
    }

    /**
     * Creation du XML ping
     */
    private function pingToXml()
    {
        $todayDate = new \DateTime("now");
        $rootNode = new \SimpleXMLElement( "<?xml version='1.0' encoding='UTF-8'?><request></request>" );

        $actionNode = $rootNode->addChild('action');
        $actionNode->addAttribute('communication', 'PING');
        $actionNode->addChild('type', 'ACK');
        $actionNode->addChild('date', $todayDate->format('Y-m-dTH:i:s.uZ'));

        $dom = new \DOMDocument();
        $dom->loadXML($rootNode->asXML());
        $dom->formatOutput = true;

        return $dom->saveXML();
    }
}
