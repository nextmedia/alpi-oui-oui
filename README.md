Alpi Keysoft Plateform
======================

Ensemble de web service permettant la connexion entre un serveur de gestion d'accession à un ensemble de licence pour une multitude de client. Chaque client ayant son parc d'utilisateurs et de pc, gérable sur une plateforme en ligne.

La génération des licences et des clients est faite par un autre web service, depuis le logiciel de gestion Alpi appelé Pikadeli vers la plateforme web.


Codes d'erreurs requête licence
-------------------------------
* **[400]** Type of action is missing
* **[400]** Emprunt value is not acceptable.
* **[401]** No more utilisation time available.
* **[401]** This licence can not be borrowed.
* **[401]** This licence can not be borrowed more than *30* days.
* **[403]** This licence can not use module *Caneco HT*
* **[403]** This licence can not be used with an old software request.
* **[404]** User Not found.
* **[404]** Software Not found.
* **[404]** PC Not found.
* **[404]** Request Module#*20* not found.
* **[404]** Request Licence#*CAN1-SKFR2069* not found.
* **[406]** Request file not found.
* **[409]** .xml or .rqt format file expected, .txt file received.
* **[419]** Too many user are using this licence.
* **[420]** Caneco One specific Module can not be borrowed. You have to borrow the complete licence.
* **[421]** You try to free a borrowed licence without borrow free request.
* **[422]** Invalid DateTime format.
* **[422]** This is not a PING request.
* **[426]** DateTime is not in an acceptable range.
* **[500]** An error occured during datetime verification.
* **[503]** Service Unavailable.


Codes d'erreurs requête Pikadeli
--------------------------------
* **[401]** Unautorized.
* **[403]** ERROR [403] Licence#CAN1-SKFR0010 trying to be reassign from client#55197 to client#ALPIint-892237621
* **[409]** Client with email *example@mail.com* already existing.
* **[423]** Client is free to be created but account registration encounter trouble.
* **[503]** Service Unavailable.


Codes de retour requête Setup Recorder
--------------------------------------
* **[211]** Without any element created.
* **[401]** Unautorized.
* **[401]** User with this iduser already exist.
* **[401]** This PC already exist for another client.
* **[404]** User not found.
* **[412]** User has not been initialized.
* **[425]** This user already exist, but with another id user.
* **[503]** Service Unavailable.
* **[513]** MultiPC key.
* **[514]** This email seems to have multiple user account.


Codes de retour requête Authentication
--------------------------------------
* **[204]** OK
* **[208]** This pc already exist
* **[209]** Missing parameter
* **[401]** KO
* **[401]** User email is required 
* **[401]** User not found
* **[503]** Service Unavailable.
