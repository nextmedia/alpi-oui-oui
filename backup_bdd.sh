cd /var/www/backups/sql

## DUMP DE LA BASE
mysqldump -uroot -pextN-63-ediaM bd_alpi > alpi_`date +"%Y-%m-%d"`.sql

## COMPRESSION DES EXPORTS
tar -czf alpi_`date +"%Y-%m-%d"`.tar.gz alpi_`date +"%Y-%m-%d"`.sql

## Suppression des fichiers non compresses
rm alpi_`date +"%Y-%m-%d"`.sql

## Suppression des archives de plus de 2 jours
find /var/www/backups/sql -type f -mtime +2 -delete

## Envoi vers FTP
ncftpput -m -u FTP-Alex -p FTP@dmin 109.237.244.9 /ovh/alpi/bdd /var/www/backups/sql/alpi_`date +"%Y-%m-%d"`.tar.gz
